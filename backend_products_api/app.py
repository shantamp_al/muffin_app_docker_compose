from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

# print(type(app))
# print(type(api))

## Code our Flask API
## We need muffin to be supplied as JSON
class Product(Resource):
    def get(self):
        return {
            "products": ["beer induced muffin","Double Chocolate", "Duffin!", "Raisin Muffin"]
        }

class Product_price(Resource):
    def get(self):
        return {
            "price": [12, 10, 17, 10]
        }
# Expose this in the API
api.add_resource(Product, '/')
api.add_resource(Product_price, '/price')

if __name__ == '__main__':
    print("THIS IS THE MAIN")
    print("Use this block to make an app have different behaviour when called directly")
    app.run("0.0.0.0", port=80, debug=True)
