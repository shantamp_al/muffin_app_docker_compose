<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shannon's Muffin Store</title>
</head>
<body>
    <h1>Welcome to my AWESOME Muffin Store</h1>
    <p>Take a look at the muffins and duffins available</p>

    <ul>
        <?php
            $json = file_get_contents('http://product-api/');
            $objects = json_decode($json);
            
            $products = $objects->products;

            foreach ($products as $product) {
                echo "<li>$product</li>";
            }
        ?>
    </ul>
    
</body>
</html>