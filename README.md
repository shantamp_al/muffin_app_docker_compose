# Muffin Compose 

This will be an app to showcase docker compose. We'll build it with a php html front page and another container with a python API simulating a DB.

We'll use this class to cover:

- Dev Environments
- Testing & CD environments
- Git and github branching
- Docker and docker compose

We will also touch on:

- Python
- API
- Webapps
- Seperation of concerns

Extra objectives:

- add ci
- add cd

### Plan to complete

1. Create a simple API with python. Put it in a container.
2. Create simple PHP app to parse/consume JSON sent from api.
3. Build our docker compose
